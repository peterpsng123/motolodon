import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import time
import pandas as pd

url = 'https://store.motoldn.com/collections/best-sellers'
options = webdriver.ChromeOptions()
#options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
#options.add_argument('--headless')
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
options.add_experimental_option("prefs", {"profile.password_manager_enabled": False, "credentials_enable_service": False})
#keyord = input("input keyword:")
driver =  webdriver.Chrome(executable_path="C:/Users/genhk/python_scipts/scraping/chromedriver.exe", options = options)
driver.set_window_size(1120,1000)
driver.get(url)